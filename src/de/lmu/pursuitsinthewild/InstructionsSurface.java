package de.lmu.pursuitsinthewild;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author mkhamis
 * 
 */
class InstructionsSurface extends JPanel {

	// the parent SmoothPursuits object
	private SmoothPursuits sp;

	JLabel instructionLabel = new JLabel(
			"To catch a fish, follow it with your eyes!");
	JLabel readyLabel = new JLabel("Try not to move your head!");
	JLabel timerLabel = new JLabel("(4)");

	// How long the instrucitons will be shown
	int timer = 4;

	public InstructionsSurface(SmoothPursuits sp) {
		this.sp = sp;
		initUI();
	}

	private void initUI() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// defining a margin
		int border = (int) (sp.getHeight() * .04);
		Color reddish = new Color(242, 117, 99);

		this.setBorder(BorderFactory.createEmptyBorder(border, border, border,
				border));

		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.3)));

		instructionLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		instructionLabel.setForeground(reddish);
		instructionLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(instructionLabel);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.06)));

		readyLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		readyLabel.setForeground(reddish);
		readyLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(readyLabel);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.02)));

		timerLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		timerLabel.setForeground(reddish);
		timerLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(timerLabel);

		this.startTimer(sp);

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(new ImageIcon("./images/underwater.jpg").getImage(), 0, 0,
				this.getWidth(), this.getHeight(), null);
	}

	public void startTimer(SmoothPursuits sp) {
		new timerRunnable(sp);
	}

	/**
	 * This runnable runs the thread responsible for the timer until the
	 * instructions disappear
	 * 
	 * @author mkhamis
	 * 
	 */
	class timerRunnable implements Runnable {
		private Thread runner;
		private MovingImage object;
		private SmoothPursuits sp;

		public timerRunnable(SmoothPursuits sp) {
			this.sp = sp;
			initThread();
		}

		private void initThread() {
			runner = new Thread(this);
			runner.start();
		}

		public void run() {
			while (timer > 0) {
				try {
					Thread.sleep(800);
					timer--;
					timerLabel.setText("(" + timer + ")");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			// call the next level 
			sp.nextLevel();
		}
	}

}