package de.lmu.pursuitsinthewild;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;

public class Test {

	public static void main(String[]args){
//		JFrame frame = new JFrame();//Make a frame
//		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
//	    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);//Make it go away on close
//	    
//	    frame.setVisible(true);//Show the frame
//
//	    
//	    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
//
//	    System.out.println(timeStamp);
//	    System.out.println(System.currentTimeMillis());
//	    System.out.println(new java.util.Date());
//	    frame.revalidate();
		
//		String s = "hi";
//		String x = s;
//		s = "hello";
//		System.out.println(x);
		
		long score = 8417;
		double d = (double )score / 1000;
		BigDecimal bd = new BigDecimal(d);
		bd = bd.round(new MathContext(3));
		double rounded = bd.doubleValue();
		System.out.println(rounded);
	}
}
