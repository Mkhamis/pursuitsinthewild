package de.lmu.pursuitsinthewild;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.math.BigDecimal;
import java.math.MathContext;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This surface is responsible for showing the recap page, in which a score is
 * shown and the game is reset after TIMER_VALUE seconds
 * 
 * @author mkhamis
 * 
 */
class EndSurface extends JPanel {

	// The parent SmoothPursuits object
	private SmoothPursuits sp;

	JLabel congratsLabel = new JLabel(
			"Your caught 'em all in: XX seconds! Well done!!");
	JLabel encourageLabel = new JLabel("Play again and beat your own score!!");

	JLabel resetLabel = new JLabel("Next game starts in 10 seconds");

	// The number of seconds in which the recap screen will be shown
	public static final int TIMER_VALUE = 5;

	// Unlike the previous static var, this one's value changes every second
	int timer = TIMER_VALUE;

	public EndSurface(SmoothPursuits sp) {
		this.sp = sp;
		initUI();
	}

	private void initUI() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		// set margins
		int border = (int) (sp.getHeight() * .04);
		Color reddish = new Color(242, 117, 99);

		this.setBorder(BorderFactory.createEmptyBorder(border, border, border,
				border));

		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.2)));

		congratsLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		congratsLabel.setForeground(reddish);
		congratsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(congratsLabel);

		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.1)));

		encourageLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		encourageLabel.setForeground(reddish);
		encourageLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(encourageLabel);

		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.2)));

		resetLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		resetLabel.setForeground(reddish);
		resetLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(resetLabel);

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(new ImageIcon("./images/underwater.jpg").getImage(), 0, 0,
				this.getWidth(), this.getHeight(), null);
	}

	public void startTimer(SmoothPursuits sp) {
		// reset the value of timer to TIMER_VALUE
		this.timer = TIMER_VALUE;
		new timerRunnable(sp);
	}

	public void updateScore() {
		long score = Long.parseLong(sp.gameEndTime)
				- Long.parseLong(sp.gameStartTime);

		System.out.println("Score= " + score);
		double d = (double) score / 1000;
		BigDecimal bd = new BigDecimal(d);
		bd = bd.round(new MathContext(3));
		double rounded = bd.doubleValue();

		congratsLabel.setText("Your caught 'em all in: " + rounded
				+ " seconds! Well done!!");
	}

	/**
	 * Runs a thread that updates the timer every 800 ms
	 * @author mkhamis
	 *
	 */
	class timerRunnable implements Runnable {
		private Thread runner;
		private MovingImage object;
		private SmoothPursuits sp;

		public timerRunnable(SmoothPursuits sp) {
			this.sp = sp;
			initThread();
		}

		private void initThread() {
			runner = new Thread(this);
			runner.start();
		}

		public void run() {
			while (timer > 0) {
				try {
					Thread.sleep(800);
					timer--;
					resetLabel.setText("Next game starts in " + timer
							+ " seconds");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("end page");
			sp.resetLevels();
		}
	}

}