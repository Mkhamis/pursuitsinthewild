package de.lmu.pursuitsinthewild;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import tobii.APIException;
import tobii.EyeTracker;
import tobii.GazeEvent;
import tobii.GazeListener;

public class SmoothPursuits extends JFrame implements KeyListener {

	private GazePoint currentGazePoint;
	// an array of horizontal gaze positions collected so far
	private ArrayList<Double> gazeXCoordinates;
	// an array of vertical gaze positions collected so far
	private ArrayList<Double> gazeYCoordinates;

	// an array of horizontal gaze positions collected so far
	private ArrayList<Double> objectXCoordinates;
	// an array of vertical gaze positions collected so far
	private ArrayList<Double> objectYCoordinates;
	private double meanXCoordinates;
	private double meanYCoordinates;

	private double stdevXCoordinates;
	private double stdevYCoordinates;

	// an array of the MovingImages to move in the current surface
	private ArrayList<MovingImage> movingImages = new ArrayList<MovingImage>();

	// A check for correlation will happen every WINDOW_SIZE ms
	private static int WINDOW_SIZE = 500;

	// The thresholds at which a correlation is considered high enough to assume
	// the user is looking at that object
	private static double LINEAR_THREASHOLD = 0.8 * 2;
	private static double CIRCULAR_THREASHOLD = 0.8 * 2;

	// a flag to pause collecting gaze data
	private boolean pauseTracking = false;

	public static final int START_PAGE = 1;
	public static final int INSTRUCTIONS_PAGE = 2;
	public static final int LEVEL_PAGE = 3;
	public static final int END_PAGE = 4;

	// The current;y shown page
	int currentPage = START_PAGE;

	// True if no user is interacting
	boolean idle = true;

	// counters used to count the time of each state. Ideally these should be
	// timers
	int idleCounter = 0;
	int activeCounter = 0;

	// the current level index
	private int nextLevel = 0;

	// the speed of images
	int px_per_second = 300;

	ImageSurface currentImageSurface;
	InstructionsSurface is;
	EndSurface es;

	int windowCounter = 0;

	StartSurface startSurface;

	private ArrayList<ImageSurface> imageSurfaces = new ArrayList<ImageSurface>();

	private static final String NEW_LINE_SEPARATOR = "\n";

	private static final String[] FILE_HEADER = { "level", "start", "remove1",
			"remove2", "end" };

	private static final String[] FILE_HEADER2 = { "level", "time", "corr1",
			"corr2" };

	private static final String[] FILE_HEADER3 = { "level", "timestamp",
			"gazeX", "gazeY", "obj1x", "obj1y", "obj2x", "obj2y" };

	// logging stuff
	FileWriter fileWriter1 = null;
	FileWriter fileWriter2 = null;
	FileWriter fileWriter3 = null;
	CSVPrinter csvFilePrinter1 = null;
	CSVPrinter csvFilePrinter2 = null;
	CSVPrinter csvFilePrinter3 = null;

	CSVFormat csvFileFormat = CSVFormat.DEFAULT
			.withRecordSeparator(NEW_LINE_SEPARATOR);

	List<String> currentCSVRow;
	List<String> currentCSVCorrelationsRow = new ArrayList<String>();
	List<String> gazeDataCSVRow = new ArrayList<String>();

	String gameStartTime = "";
	String gameEndTime = "";

	public SmoothPursuits() {
		addKeyListener(this);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setUndecorated(true);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		createAnimations();

		// define a startSurface and an endSurface
		startSurface = new StartSurface(this);
		es = new EndSurface(this);
		// Set current surface to stsrtSurface
		this.add(startSurface);

		this.currentGazePoint = new GazePoint();

		this.gazeXCoordinates = new ArrayList<Double>();
		this.gazeYCoordinates = new ArrayList<Double>();

		this.objectXCoordinates = new ArrayList<Double>();
		this.objectYCoordinates = new ArrayList<Double>();

		createGazeCSV();
	}

	/**
	 * Creates the animations
	 */
	public void createAnimations() {

		// define lists of moving images, each list is used for a separate level
		ArrayList<MovingImage> movingImages1 = new ArrayList<MovingImage>();
		ArrayList<MovingImage> movingImages2 = new ArrayList<MovingImage>();
		ArrayList<MovingImage> movingImages3 = new ArrayList<MovingImage>();
		ArrayList<MovingImage> movingImages4 = new ArrayList<MovingImage>();
		ArrayList<MovingImage> movingImages5 = new ArrayList<MovingImage>();
		ArrayList<MovingImage> movingImages6 = new ArrayList<MovingImage>();
		ArrayList<MovingImage> movingImages7 = new ArrayList<MovingImage>();
		ArrayList<MovingImage> movingImages8 = new ArrayList<MovingImage>();

		// define the MovingImages (fish)
		int fishSize = this.getWidth() / 10;
		MovingImage fish1 = new MovingImage(this.getWidth() / 4,
				this.getHeight() / 3, fishSize, fishSize,
				"./images/colorful.png", "Colorful");
		MovingImage fish2 = new MovingImage(this.getWidth() * 3 / 4,
				this.getHeight() * 3 / 4, fishSize, fishSize,
				"./images/normal.png", "Normal");

		MovingImage fish3 = new MovingImage(this.getWidth() / 2,
				this.getHeight() / 3, fishSize, fishSize, "./images/scary.png",
				"Scary");
		MovingImage fish4 = new MovingImage(this.getWidth() * 3 / 50,
				this.getHeight() * 3 / 4, fishSize, fishSize,
				"./images/normal.png", "Normal");

		MovingImage fish5 = new MovingImage(this.getWidth() * 9 / 25,
				this.getHeight() / 2, fishSize, fishSize,
				"./images/colorful.png", "Colorful");
		MovingImage fish6 = new MovingImage(this.getWidth() * 2 * 9 / 25,
				this.getHeight() * 1 / 4, fishSize, fishSize,
				"./images/scary.png", "Scary");

		MovingImage fish7 = new MovingImage(this.getWidth() * 9 / 25,
				this.getHeight() * 6 / 10, fishSize, fishSize,
				"./images/colorful.png", "Colorful");
		MovingImage fish8 = new MovingImage(this.getWidth() * 2 * 9 / 25,
				this.getHeight() * 6 / 10, fishSize, fishSize,
				"./images/normal.png", "Normal");

		// Add the MovingImages to the lists, any list can have multiple fish

		movingImages1.add(fish1);
		// movingImages1.add(fish2);
		movingImages5.add(fish2);

		movingImages2.add(fish3);
		movingImages6.add(fish4);

		movingImages3.add(fish5);
		movingImages7.add(fish6);

		movingImages4.add(fish7);
		movingImages8.add(fish8);

		// 1: fast linear
		// 2: slow linear
		// 3: slow circular
		// 4: fast circular
		// 5: fast linear
		// 6: slow linear
		// 7: slow circular
		// 8: fast circular

		// Define the ImageSurfaces, each represents a stage
		ImageSurface imageSurface1 = new ImageSurface(this, movingImages1, 1);
		ImageSurface imageSurface2 = new ImageSurface(this, movingImages2, 2);
		ImageSurface imageSurface3 = new ImageSurface(this, movingImages3, 3);
		ImageSurface imageSurface4 = new ImageSurface(this, movingImages4, 4);
		ImageSurface imageSurface5 = new ImageSurface(this, movingImages5, 5);
		ImageSurface imageSurface6 = new ImageSurface(this, movingImages6, 6);
		ImageSurface imageSurface7 = new ImageSurface(this, movingImages7, 7);
		ImageSurface imageSurface8 = new ImageSurface(this, movingImages8, 8);

		// The distance that the MovingImage will move
		int distance = this.getWidth() / 3;

		// Define the linear animations
		imageSurface1.animate(fish1, fish1.currentX + distance, fish1.currentY
				+ distance, 650, true);
		imageSurface5.animate(fish2, fish2.currentX - distance, fish2.currentY
				- distance, 650, true);

		imageSurface2.animate(fish3, fish3.currentX - distance, fish3.currentY
				+ distance, 450, true);
		imageSurface6.animate(fish4, fish4.currentX + distance, fish4.currentY
				- distance, 450, true);

		// Define the circular animations
		int cycleRadius = this.getWidth() / 6;
		imageSurface3.circularAnimation(fish5, 30, 450, -cycleRadius);
		imageSurface7.circularAnimation(fish6, 30, 450, cycleRadius);

		imageSurface4.circularAnimation(fish7, 30, 650, cycleRadius);
		imageSurface8.circularAnimation(fish8, 30, 650, -cycleRadius);

		// Add the ImageSurfaces to the imageSurfaces list of the current
		// SmoothPursuits object
		imageSurfaces.add(imageSurface1);
		imageSurfaces.add(imageSurface2);
		imageSurfaces.add(imageSurface3);
		imageSurfaces.add(imageSurface4);
		imageSurfaces.add(imageSurface5);
		imageSurfaces.add(imageSurface6);
		imageSurfaces.add(imageSurface7);
		imageSurfaces.add(imageSurface8);

		// shuffle their order
		Collections.shuffle(imageSurfaces);
	}

	/**
	 * Pause collecting gaze data
	 */
	public void pauseEyeTracking() {
		this.pauseTracking = true;
	}

	/**
	 * Unpause collecting gaze data
	 */
	public void unpauseEyeTracking() {
		this.pauseTracking = false;
	}

	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				// initialize SmoothPursuits object
				SmoothPursuits mainFrame = new SmoothPursuits();

				// Define an EyeTracker object
				EyeTracker tracker;
				try {
					tracker = new EyeTracker();

					final GazeListener listener = new GazeListener() {

						@Override
						public void gazeEvent(GazeEvent event) {

							double x = event.center().gazeOnDisplayNorm.x();
							double y = event.center().gazeOnDisplayNorm.y();

							mainFrame.currentGazePoint = new GazePoint(x, y);

							if (x > -1 && y > -1) {
								// Add the current GazePoint to the list of
								// collected GazePoints
								mainFrame
										.addGazeCoordinates(mainFrame.currentGazePoint);

								// prepare the data for logging
								mainFrame.gazeDataCSVRow.add(String
										.valueOf(System.currentTimeMillis()));
								mainFrame.gazeDataCSVRow.add(String.valueOf(x));
								mainFrame.gazeDataCSVRow.add(String.valueOf(y));

								// Add the current coordinates of each shown
								// object to the list of collected coordinates
								for (MovingImage movingImage : mainFrame.movingImages) {
									movingImage.logCoordinates();
									mainFrame.gazeDataCSVRow.add(String
											.valueOf((double) movingImage.currentX
													/ mainFrame.getWidth()));
									mainFrame.gazeDataCSVRow.add(String
											.valueOf((double) movingImage.currentY
													/ mainFrame.getHeight()));
								}
								// log the gaze data
								mainFrame.recordGazeToCSV();

								mainFrame.repaint();
							}
							boolean leftDetected = event.left.eyePosFromTrackerMM
									.x() != 0;
							boolean rightDetected = event.right.eyePosFromTrackerMM
									.x() != 0;
							boolean bothDetected = mainFrame.startSurface
									.setEyesDetected(leftDetected,
											rightDetected);
							if ((mainFrame.idle || mainFrame.currentPage == mainFrame.START_PAGE)
									&& bothDetected) {
								// This is what happens when eye's are detected
								// but current state is idle
								mainFrame.activeCounter++;
								if (mainFrame.activeCounter >= 50) {
									mainFrame.idleCounter = 0;
									mainFrame.activeCounter = 0;
									mainFrame.nextLevel();
									mainFrame.idle = false;
								}

							} else if (!mainFrame.idle && !bothDetected) {
								// This is what happens when no eyes detected
								// and current state is active (!idle)
								mainFrame.idleCounter++;
								if (mainFrame.idleCounter >= 50) {
									mainFrame.setImageSurfaceBlinking(true);
								}
								if (mainFrame.idleCounter >= 200) {
									mainFrame.idleCounter = 0;
									mainFrame.activeCounter = 0;
									mainFrame.resetLevels();
									mainFrame.idle = true;
								}
							} else if (bothDetected) {
								// This is called any time the eyes are
								// detected, whether midgame or homepage
								mainFrame.setImageSurfaceBlinking(false);
							}
						}

						@Override
						public void apiException(APIException exception) {
							System.out.println(exception);
						}
					};
					tracker.connect().register(listener).start();

					// checks the correlation between collected gaze data and
					// image coordinates
					Runnable checkCorrelation = new Runnable() {
						public void run() {
							try {
								if (!mainFrame.pauseTracking) {

									mainFrame.windowCounter++;
									// x is the image with highest correlation
									MovingImage x = mainFrame
											.returnHighestCorrelationImage();
									if (x != null
											&& ((!x.circularMotion && x.correlation > LINEAR_THREASHOLD) || (x.circularMotion && x.correlation > CIRCULAR_THREASHOLD))) {

										// if the correlation is higher than the
										// threshold, reset the window counter
										mainFrame.windowCounter = 0;

										// System.out
										// .println("=======================================================");
										// System.out.println("Max is: " +
										// x.name
										// + " with value "
										// + x.correlation);
										//
										// System.out
										// .println("=======================================================");

										if (x != null) {
											// fade out the object with highest
											// correlation
											mainFrame.currentImageSurface
													.fadeOut(x, mainFrame);
										}
									}
									// System.out.println(x.name +
									// " with value "
									// + x.correlation);

									// clear all coordinates collected during
									// the last window
									mainFrame.clearCoordinates();

									// if this is a level page, then log the
									// correlations
									if (mainFrame.currentPage == mainFrame.LEVEL_PAGE) {
										mainFrame.csvFilePrinter2
												.printRecord(mainFrame.currentCSVCorrelationsRow);
										mainFrame.fileWriter2.flush();

									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					};

					// execute the check for correlation every WINDOW_SIZE ms
					ScheduledExecutorService executor = Executors
							.newScheduledThreadPool(1);
					executor.scheduleAtFixedRate(checkCorrelation, WINDOW_SIZE,
							WINDOW_SIZE, TimeUnit.MILLISECONDS);

				} catch (APIException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * add the gazepoint to the collected gaze coordinates
	 * 
	 * @param gazePoint
	 */
	public void addGazeCoordinates(GazePoint gazePoint) {
		addGazeCoordinates(gazePoint.getX(), gazePoint.getY());
	}

	/**
	 * add the x and y to the collected gaze coordinates
	 * 
	 * @param x
	 * @param y
	 */
	public void addGazeCoordinates(double x, double y) {
		this.gazeXCoordinates.add(new Double(x));
		this.gazeYCoordinates.add(new Double(y));
	}

	/**
	 * Clear up all gathered coordinates
	 */
	public void clearCoordinates() {

		this.gazeXCoordinates = new ArrayList<Double>();
		this.gazeYCoordinates = new ArrayList<Double>();

		for (MovingImage movingImage : this.movingImages) {
			movingImage.clearCoordinates();
		}
	}

	public void reset() {
		this.clearCoordinates();
		// this.movingObjectX = 0.0;
		// this.movingObjectY = 0.0;
		this.getGraphics().clearRect(0, 0, getWidth(), getHeight());
		repaint();
	}

	/**
	 * 
	 * @return The Pearson Product-Moment Correlation between the given Object
	 *         and the current gaze data.
	 */
	public Double[] getCorrelation(MovingImage movingImage) {
		return getCorrelation(movingImage.getObjectXCoordinates(),
				movingImage.getObjectYCoordinates());
	}

	/**
	 * 
	 * @return The Pearson Product-Moment Correlation between the two given
	 *         lists.
	 */
	@SuppressWarnings("finally")
	public Double[] getCorrelation(ArrayList<Double> l1, ArrayList<Double> l2) {

		// gazeXCoordinates.add(1.2);
		// gazeXCoordinates.add(2.3);
		// gazeXCoordinates.add(3.8);
		// gazeXCoordinates.add(4.6);
		// gazeXCoordinates.add(5.7);
		// gazeXCoordinates.add(6.3);
		//
		// gazeYCoordinates.add(6.3);
		// gazeYCoordinates.add(5.7);
		// gazeYCoordinates.add(4.6);
		// gazeYCoordinates.add(3.8);
		// gazeYCoordinates.add(2.3);
		// gazeYCoordinates.add(1.2);
		//
		// objectXCoordinates.add(1.2);
		// objectXCoordinates.add(2.3);
		// objectXCoordinates.add(3.8);
		// objectXCoordinates.add(4.6);
		// objectXCoordinates.add(5.7);
		// objectXCoordinates.add(6.3);
		//
		// objectYCoordinates.add(6.3);
		// objectYCoordinates.add(5.7);
		// objectYCoordinates.add(4.6);
		// objectYCoordinates.add(3.8);
		// objectYCoordinates.add(2.3);
		// objectYCoordinates.add(1.2);

		// only consider the list when its size is more than 3
		if (gazeXCoordinates.size() < 3 || gazeYCoordinates.size() < 3)
			return new Double[] { 0.0, 0.0 };

		double[] gazeX = new double[gazeXCoordinates.size()];
		double[] gazeY = new double[gazeYCoordinates.size()];

		// double[] objectX = new double[objectXCoordinates.size()];
		// double[] objectY = new double[objectYCoordinates.size()];

		double[] objectX = new double[l1.size()];
		double[] objectY = new double[l2.size()];

		// System.out.println(gazeX.length);
		// System.out.println(gazeY.length);
		// System.out.println(objectX.length);
		// System.out.println(objectY.length);

		Double[] result = { 0.0, 0.0 };

		// put the coordinates in primitive arrays
		try {
			for (int i = 0; i < gazeX.length; i++) {
				gazeX[i] = gazeXCoordinates.get(i) * this.getWidth();
				gazeY[i] = gazeYCoordinates.get(i) * this.getHeight();
				objectX[i] = l1.get(i);
				objectY[i] = l2.get(i);

				// System.out.println(gazeX[i] + "\t" + gazeY[i] + "\t" +
				// objectX[i]
				// + "\t" + objectY[i]);

			}

			// System.out.println(windowCounter + ") " + gazeY.length + "   " +
			// objectY.length);

			// System.out.println(gazeX.length);
			PearsonsCorrelation ps = new PearsonsCorrelation();

			result = new Double[] { ps.correlation(gazeX, objectX),
					ps.correlation(gazeY, objectY) };
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

		// try {
		// loadCoordinateStats();
		// } catch (Exception e) {
		// e.printStackTrace();
		// return 0.0;
		// }
		// for (int i = 0; i < this.gazeXCoordinates.size(); i++) {
		// double gazeXCoordinate = this.gazeXCoordinates.get(i);
		// double gazeYCoordinate = this.gazeYCoordinates.get(i);
		//
		// double corrX = Math.abs((gazeXCoordinate - this.meanXCoordinates)
		// * (gazeXCoordinate - this.meanXCoordinates))
		// / (this.stdevXCoordinates * this.stdevXCoordinates);
		//
		// double corrY = Math.abs((gazeYCoordinate - this.meanYCoordinates)
		// * (gazeYCoordinate - this.meanYCoordinates))
		// / (this.stdevYCoordinates * this.stdevYCoordinates);
		//
		// corrXArray.add(corrX);
		// corrYArray.add(corrY);
		// }
		//
		// return 0;
	}

	/**
	 * Calculates the averages of the horizontal and vertical coordinates, and
	 * loads them into the local variables <code>averageXCoordinates</code> and
	 * <code>averageYCoordinates</code>
	 * 
	 * @throws Exception
	 *             if the number of horizontal coordinates is not equal to the
	 *             vertical ones
	 */
	public void loadCoordinateStats() throws Exception {

		if (meanXCoordinates > 0.0 && meanYCoordinates > 0.0
				&& stdevXCoordinates > 0.0 && stdevYCoordinates > 0.0) {
			// The stats are already loaded, nothing to do here..
			return;
		}

		if (!gazeXCoordinates.isEmpty() && !gazeYCoordinates.isEmpty()) {
			if (gazeXCoordinates.size() == gazeYCoordinates.size()) {

				// The descriptive statistics package works only with double[],
				// hence the following loop
				double[] targetX = new double[gazeXCoordinates.size()];
				double[] targetY = new double[gazeXCoordinates.size()];
				for (int i = 0; i < targetX.length; i++) {
					targetX[i] = gazeXCoordinates.get(i);
					targetY[i] = gazeYCoordinates.get(i);
				}

				DescriptiveStatistics dsX = new DescriptiveStatistics(targetX);
				meanXCoordinates = dsX.getMean();
				stdevXCoordinates = dsX.getStandardDeviation();

				DescriptiveStatistics dsY = new DescriptiveStatistics(targetY);
				meanYCoordinates = dsY.getMean();
				stdevYCoordinates = dsY.getStandardDeviation();
			} else {
				// The numbers of coordinates on each axis are unequal,
				// something is wrong here..
				throw new Exception(gazeXCoordinates.size()
						+ " horizontal points were collected, while "
						+ gazeYCoordinates.size() + " were collected.");
			}
		}
	}

	/**
	 * Draws a sphere where the gazepoint is estimated to be
	 * 
	 * @param dX
	 * @param dY
	 */
	public void drawGazeCircle(double dX, double dY) {

		int x = (int) (dX * this.getWidth());
		int y = (int) (dY * this.getHeight());

		int diameter = 20;
		Graphics g = this.getGraphics();
		g.setColor(Color.BLACK);
		g.fillOval(x - diameter / 2, y - diameter / 2, diameter, diameter);

		// System.out.println(this.currentGazePoint.getX());
		// System.out.println(this.currentGazePoint.getY());
		//
		// System.out.println(x);
		// System.out.println(y);
		//
		// System.out.println("------------");
	}

	// public void drawMovingObject() {
	// Graphics g = this.getGraphics();
	// g.setColor(Color.RED);
	// g.fillOval((int) movingObjectX, (int) movingObjectY, 15, 15);
	// this.movingObjectX += 5.0;
	// this.movingObjectY += 5.0;
	// }

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		drawGazeCircle(this.currentGazePoint.getX(),
				this.currentGazePoint.getY());
		// this.movingObject.draw();
		// drawMovingObject();
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		switch (arg0.getKeyChar()) {
		case 'c':
			System.exit(0);
			break;
		case 'r':
			this.resetLevels();
			break;

		case 'f':
			this.startSurface.setLeftDetected(true);
			break;
		default:
			break;
		}

	}

	/**
	 * 
	 * @return the MovingImage with the highest correlation with the gaze data
	 */
	public MovingImage returnHighestCorrelationImage() {
		int i = 0;

		// log the correlation data into the csv file
		if (this.currentPage == LEVEL_PAGE) {
			currentCSVCorrelationsRow = new ArrayList<String>();
			// Log the index of this condition (e.g. Condition 1 = fast linear,
			// Condition 3 = slow circular)
			currentCSVCorrelationsRow.add(String.valueOf(currentImageSurface
					.getConditionIndex()));
			// Log the time correlation calculation started.
			currentCSVCorrelationsRow.add(String.valueOf(System
					.currentTimeMillis()));
		}
		ArrayList<Double> allCorrelations = new ArrayList<Double>();

		// get the correlation of every MovingImage with the gaze data
		for (MovingImage movingImage : this.movingImages) {
			Double[] correlation = getCorrelation(movingImage);
			allCorrelations.add((correlation[0].isNaN() ? 0 : correlation[0])
					+ (correlation[1].isNaN() ? 0 : correlation[1]));

			// System.out.println("Correlation (" + (int) (i + 1) + "): "
			// + allCorrelations.get(i));
			movingImage.correlation = allCorrelations.get(i);
			// System.out.println(movingObject.name
			// +": "+movingObject.correlation);
			i++;
			// if (correlation[0] > 0 && correlation[1] > 0) {
			// System.out.println("Correlation (" + i + " x): "
			// + correlation[0]);
			// System.out.println("Correlation (" + i + " y): "
			// + correlation[1]);
			// }
			// for (int j = 0; j < gazeXCoordinates.size(); j++) {
			// System.out.println(gazeXCoordinates.get(j)*this.getWidth() +
			// "\t"
			// + gazeYCoordinates.get(j)*this.getHeight() + "\t"
			// + movingObject.getObjectXCoordinates().get(j)
			// + "\t"
			// + movingObject.getObjectYCoordinates().get(j));
			// }

			// System.out
			// .println("=======================================================");
			currentCSVCorrelationsRow.add(String
					.valueOf(movingImage.correlation));
		}
		MovingImage x = MovingImage.getMaxCorrelation(this.movingImages);
		return x;
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * Removes the Image and logs its removal time
	 * 
	 * @param object
	 */
	public void removeImage(MovingImage object) {
		// this.movingImages.remove(object);
		// TODO: currentCSVLine.addRemoveTime(current_time)
		String remove = String.valueOf(System.currentTimeMillis());
		this.currentCSVRow.add(remove);

		object.deleted = true;
		if (nextLevelCheck())
			this.nextLevel();

	}

	/**
	 * Checks if it is time to move to the next level
	 * 
	 * @return true if the game should proceed to the next level i.e. all
	 *         MovingImages have been selected and removed
	 */
	public boolean nextLevelCheck() {
		for (MovingImage movingObject : movingImages) {
			if (movingObject.deleted == false)
				return false;
		}
		return true;
	}

	/**
	 * Loads the next level
	 */
	public void nextLevel() {
		this.pauseEyeTracking();

		if (this.currentPage == START_PAGE) {
			// if we're in the start page, go to the instructions page
			// This happens only before the first level
			this.getContentPane().remove(this.startSurface);
			this.is = new InstructionsSurface(this);
			this.getContentPane().add(is);
			this.revalidate();
			this.currentPage = INSTRUCTIONS_PAGE;
			return;
		} else if (this.currentPage == INSTRUCTIONS_PAGE) {
			// if we're in the instructions page, go to the first level page
			createCSVFile();
			this.getContentPane().remove(is);
			this.revalidate();
			this.currentPage = LEVEL_PAGE;
			this.gameStartTime = "";
			this.gameEndTime = "";
		}

		if (this.currentImageSurface != null) {
			this.getContentPane().remove(this.currentImageSurface);
		}

		if (nextLevel < this.imageSurfaces.size()) {
			// if you haven't gone through all ImageSurfaces yet..
			logEndTimeCSV();
			this.currentImageSurface = this.imageSurfaces.get(nextLevel);
			this.movingImages = this.currentImageSurface.movingImages;
			this.add(this.currentImageSurface);
			// for (MovingImage movingImage :
			// this.currentImageSurface.movingImages) {
			// movingImage.resetImage();
			// }
			this.revalidate();
			nextLevel++;
			logNewLevelCSV();
		} else {
			// Otherwise, then reset the game
			logEndTimeCSV();
			flushCSV();
			this.currentPage = END_PAGE;
			this.getContentPane().add(this.es);
			this.es.updateScore();
			this.es.startTimer(this);
			this.revalidate();
		}

		this.clearCoordinates();
		this.unpauseEyeTracking();

	}

	/**
	 * Log the gazedata into the corresponding csv file
	 */
	public void recordGazeToCSV() {
		try {
			csvFilePrinter3.printRecord(gazeDataCSVRow);
			gazeDataCSVRow.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the csv file responsible for storing the gaze data, or read the
	 * existing one
	 */
	public void createGazeCSV() {
		try {
			fileWriter3 = new FileWriter("gazeData.csv", true);
			csvFilePrinter3 = new CSVPrinter(fileWriter3, csvFileFormat);
			csvFilePrinter3.printRecord(FILE_HEADER3);
			gazeDataCSVRow = new ArrayList<String>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates csv files with the current timestamp, one for recording the times
	 * and another one for recording all the calculated correlations
	 */
	public void createCSVFile() {
		try {
			String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss")
					.format(new Date());
			fileWriter1 = new FileWriter("time_" + timeStamp + ".csv");
			fileWriter2 = new FileWriter("corr_" + timeStamp + ".csv");
			csvFilePrinter1 = new CSVPrinter(fileWriter1, csvFileFormat);
			csvFilePrinter1.printRecord(FILE_HEADER);
			csvFilePrinter2 = new CSVPrinter(fileWriter2, csvFileFormat);
			csvFilePrinter2.printRecord(FILE_HEADER2);

			currentCSVRow = new ArrayList<String>();
			currentCSVCorrelationsRow = new ArrayList<String>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Log end of this level
	 */
	public void logEndTimeCSV() {
		if (!currentCSVRow.isEmpty()) {
			String end = String.valueOf(System.currentTimeMillis());
			currentCSVRow.add(end); // [csv]
									// end:
									// in
									// ms
			this.gameEndTime = end;
			try {
				csvFilePrinter1.printRecord(currentCSVRow);
			} catch (IOException e) {
				e.printStackTrace();
			}
			currentCSVRow.clear();
		}
	}

	public void logNewLevelCSV() {
		currentCSVRow = new ArrayList<String>();
		// log the index of the condition (e.g. 1: fast linear, 3 slow circular)
		currentCSVRow.add(String.valueOf(currentImageSurface
				.getConditionIndex()));
		String start = String.valueOf(System.currentTimeMillis());
		// log the start time of the level
		currentCSVRow.add(start);
		if (this.gameStartTime.isEmpty())
			this.gameStartTime = start;
	}

	/**
	 * Flush the csv files to make sure all records are stored on disk
	 */
	public void flushCSV() {
		try {
			fileWriter1.flush();
			fileWriter1.close();
			csvFilePrinter1.close();
			fileWriter2.flush();
			fileWriter2.close();
			csvFilePrinter2.close();
			fileWriter3.flush();
			// fileWriter3.close();
			// csvFilePrinter3.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void resetLevels() {
		System.out.println("resetting");
		flushCSV();
		this.pauseEyeTracking();
		if (this.currentPage == END_PAGE) {
			this.getContentPane().remove(this.es);
			this.revalidate();
		} else if (this.currentPage == LEVEL_PAGE) {
			this.getContentPane().remove(this.currentImageSurface);
			this.revalidate();
		}
		this.currentPage = START_PAGE;
		nextLevel = 0;
		this.getContentPane().add(this.startSurface);

		this.imageSurfaces.clear();
		createAnimations();

		this.revalidate();
		this.clearCoordinates();
		this.unpauseEyeTracking();

	}

	public void setImageSurfaceBlinking(boolean doBlink) {
		if (this.currentImageSurface == null) {
			return;
		}
		if (this.currentImageSurface.isBlinking() && !doBlink) {
			this.currentImageSurface.setBlinking(false);
		} else if (!this.currentImageSurface.isBlinking() && doBlink) {
			this.currentImageSurface.setBlinking(true);
		}

	}

}
