package de.lmu.pursuitsinthewild;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

class MovingObject extends Ellipse2D.Float {

	Color color;
	int originalX;
	int originalY;
	
	boolean deleted = false;
	
	boolean circularMotion = false;

	float alpha;

	String name = "";

	static int counter = 1;
	int index;
	java.lang.Double correlation;
	
	// an array of horizontal gaze positions collected so far
	private ArrayList<java.lang.Double> objectXCoordinates;
	// an array of vertical gaze positions collected so far
	private ArrayList<java.lang.Double> objectYCoordinates;

	public MovingObject(float x, float y, float width, float height) {

		setFrame(x, y, width, height);
	}

	public MovingObject(float x, float y, float width, float height, Color color) {

		setFrame(x, y, width, height);
		this.name = "";
		this.color = color;
		this.alpha = 1f;
		this.originalX = (int) x;
		this.originalY = (int) y;
		this.objectXCoordinates = new ArrayList<java.lang.Double>();
		this.objectYCoordinates = new ArrayList<java.lang.Double>();
		this.index = MovingObject.counter;
		MovingObject.counter++;
		

	}

	/**
	 * 
	 * @param x position on the x-axis
	 * @param y position on the y-axis
	 * @param width width of the object
	 * @param height height of the object
	 * @param color color of the object
	 * @param name a given string name, for identification when debugging
	 */
	public MovingObject(float x, float y, float width, float height,
			Color color, String name) {

		setFrame(x, y, width, height);
		this.name = name;
		this.color = color;
		this.alpha = 1f;
		this.originalX = (int) x;
		this.originalY = (int) y;
		this.objectXCoordinates = new ArrayList<java.lang.Double>();
		this.objectYCoordinates = new ArrayList<java.lang.Double>();
		this.index = MovingObject.counter;
		MovingObject.counter++;

	}

	public ArrayList<java.lang.Double> getObjectXCoordinates() {
		return objectXCoordinates;
	}

	public ArrayList<java.lang.Double> getObjectYCoordinates() {
		return objectYCoordinates;
	}

	public void logCoordinates() {
		this.objectXCoordinates.add(new java.lang.Double(this.x));
		this.objectYCoordinates.add(new java.lang.Double(this.y));
	}

	public void clearCoordinates() {
		this.objectXCoordinates = new ArrayList<java.lang.Double>();
		this.objectYCoordinates = new ArrayList<java.lang.Double>();
	}

	public boolean isHit(float x, float y) {

		if (getBounds2D().contains(x, y)) {

			return true;
		} else {

			return false;
		}
	}

	public void addX(float x) {

		this.x += x;
	}

	public void addY(float y) {

		this.y += y;
	}

	public void addWidth(float w) {

		this.width += w;
	}

	public void addHeight(float h) {

		this.height += h;
	}

	public static MovingObject getMaxCorrelation(
			ArrayList<MovingObject> movingObjects) {
		if (movingObjects.size() == 0)
			return null;
		MovingObject maxSoFar = movingObjects.get(0);
		for (MovingObject movingObject : movingObjects) {
			if (movingObject.correlation > maxSoFar.correlation)
				maxSoFar = movingObject;
		}
		return maxSoFar;

	}
}
