package de.lmu.pursuitsinthewild;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.math3.analysis.function.Cos;

/**
 * This class represents a panel with moving images
 * 
 * @author mkhamis
 * 
 */
class ImageSurface extends JPanel {

	// The list of MovingImages that are to be displayed in this page
	ArrayList<MovingImage> movingImages;

	// The parent SmoothPursuits object
	private SmoothPursuits sp;

	// The following variables are used in case no eyes are detected
	static String NONE_EYES = "No Eyes Detected";

	JLabel eyesDetectedLabel = new JLabel(NONE_EYES);
	JLabel eyesDetectedHint = new JLabel(
			"Please step into the green marker on the floor");

	BlinkingRunnable br;

	private int conditionIndex;

	public ImageSurface() {

		initUI();
	}

	/**
	 * Initializes a new ImageSurface
	 * 
	 * @param sp
	 *            The parent SmoothPursuits object
	 * @param movingImages
	 *            the list of MovingImages that will be shown in this panel
	 * @param conditionIndex
	 *            a numeral index that I used for debugging, it represents the
	 *            condition e.g. 1 is the condition of moving linearly at a fast
	 *            speed
	 */
	public ImageSurface(SmoothPursuits sp, ArrayList<MovingImage> movingImages,
			int conditionIndex) {

		this.movingImages = movingImages;
		this.sp = sp;
		this.conditionIndex = conditionIndex;
		initUI();
	}

	/**
	 * Animates the given object
	 * 
	 * @param movingObject
	 *            the object to be animated
	 * @param targetX
	 *            the point on the X-axis (in pixels) to which the object should
	 *            move to
	 * @param targetY
	 *            the point on the Y-axis (in pixels) to which the object should
	 *            move to
	 * @param px_per_second
	 *            the numbers of pixels covered per second
	 * @param facingRight
	 *            true if the original image is facing right
	 */
	public void animate(MovingImage movingObject, int targetX, int targetY,
			int px_per_second, boolean facingRight) {
		new AnimateRunnable(movingObject, targetX, targetY, px_per_second,
				facingRight);
	}

	/**
	 * Animates the given object
	 * 
	 * @param movingObject
	 *            the object to be animated
	 * @param targetX
	 *            the point on the X-axis (in pixels) to which the object should
	 *            move to
	 * @param targetY
	 *            the point on the Y-axis (in pixels) to which the object should
	 *            move to
	 * @param px_per_second
	 *            the numbers of pixels covered per second
	 * @param facingRight
	 *            true if the original image is facing right
	 */
	public void animate(MovingImage movingObject, double targetX,
			double targetY, int px_per_second, boolean facingRight) {
		animate(movingObject, (int) targetX, (int) targetY, px_per_second,
				facingRight);
	}

	/**
	 * Fades out the given MovingImage
	 * 
	 * @param movingObject
	 *            The image to be faded out
	 * @param mainFrame
	 *            the parent SmoothPursuit object
	 */
	public void fadeOut(MovingImage movingObject, SmoothPursuits mainFrame) {
		new FadeOutRunnable(movingObject, mainFrame);
	}

	private void initUI() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		// zrect = new ZRectangle(50, 50, 50, 50);

		// zell = new ZEllipse(150, 70, 50, 50);
		// zell2 = new ZEllipse(600, 800, 50, 50);
		int border = (int) (sp.getHeight() * .04);
		Color reddish = new Color(242, 117, 99);
		Color orangish = new Color(255, 179, 57);

		this.setBorder(BorderFactory.createEmptyBorder(border, border, border,
				border));

		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.8)));

		eyesDetectedLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		eyesDetectedLabel.setForeground(reddish);
		eyesDetectedLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		eyesDetectedLabel.setVisible(false);
		this.add(eyesDetectedLabel);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.04)));

		eyesDetectedHint.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		eyesDetectedHint.setForeground(reddish);
		eyesDetectedHint.setAlignmentX(Component.CENTER_ALIGNMENT);
		eyesDetectedHint.setVisible(false);
		this.add(eyesDetectedHint);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.04)));

	}

	public void setBlinking(boolean doBlink) {
		if (doBlink) {
			if (br == null) {
				this.br = new BlinkingRunnable(eyesDetectedLabel,
						eyesDetectedHint);
			} else if (!br.isAlive()) {
				br.notify();
			}

		} else if (!doBlink) {
			if (br != null && br.isAlive()) {
				br.pause();
			}
		}
	}

	/**
	 * 
	 * @return true if the "no eyes" error message is blinking
	 */
	public boolean isBlinking() {
		return this.br != null && this.br.isAlive();
	}

	private void doDrawing(Graphics g) {
		g.drawImage(new ImageIcon("./images/underwater.jpg").getImage(), 0, 0,
				this.getWidth(), this.getHeight(), null);
		Graphics2D g2d = (Graphics2D) g;

		for (MovingImage movingImage : movingImages) {
			try {
				g2d.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, movingImage.alpha));
				g2d.setPaint(Color.red);
			} catch (ConcurrentModificationException e) {
				e.printStackTrace();
			}
			g.drawImage(movingImage.getImage(), (int) movingImage.currentX,
					(int) movingImage.currentY, null);
		}
	}

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		doDrawing(g);
	}

	/**
	 * This runnable runs the thread responsible for moving the object in a
	 * linear trajectory
	 * 
	 * @author mkhamis
	 * 
	 */
	class AnimateRunnable implements Runnable {

		private Thread runner;
		private MovingImage object;
		private int targetX;
		private int targetY;
		private int px_per_second;
		private boolean facingRight = false;

		/**
		 * 
		 * @param object
		 *            the MovingImage to be animated
		 * @param targetX
		 *            the target position on the x-axis
		 * @param targetY
		 *            the target position on the y-axis
		 * @param facingRight
		 *            true of the image faces the right direction
		 */
		public AnimateRunnable(MovingImage object, int targetX, int targetY,
				boolean facingRight) {
			this.object = object;
			this.targetX = targetX;
			this.targetY = targetY;
			this.facingRight = facingRight;
			initThread();
		}

		public AnimateRunnable(MovingImage object, int targetX, int targetY,
				int px_per_second, boolean facingRight) {
			this.object = object;
			this.targetX = targetX;
			this.targetY = targetY;
			this.px_per_second = px_per_second;
			this.facingRight = facingRight;
			initThread();
		}

		private void initThread() {

			runner = new Thread(this);
			runner.start();
		}

		/**
		 * Flips the image to the other direction
		 * 
		 * @param image
		 * @return
		 */
		private BufferedImage flipHorizontal(BufferedImage image) {
			this.facingRight = !this.facingRight;
			AffineTransform at = new AffineTransform();
			at.concatenate(AffineTransform.getScaleInstance(-1, 1));
			at.concatenate(AffineTransform.getTranslateInstance(
					-image.getWidth(null), 0));

			BufferedImage newImage = new BufferedImage(image.getWidth(),
					image.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = newImage.createGraphics();
			g.transform(at);
			g.drawImage(image, 0, 0, null);
			g.dispose();
			return newImage;
		}

		/**
		 * This runs every 10 ms, so if the px_per_second is 500, this means
		 * that it will move the object 5 px every 10 ms, and thus 5*100 px
		 * every 10*100 ms. Which is again 500 px per 1 second
		 */
		public void run() {
			// The following flags are used to handle the orientation of the
			// image, and also for swapping the origin and source points when
			// the MovingImage reaches any of them
			boolean isRight = false;
			boolean isLeft = false;
			boolean isDown = false;
			boolean isUp = false;
			if (object.currentX < targetX)
				isRight = true;
			else if (object.currentX > targetX)
				isLeft = true;

			if (object.currentY < targetY)
				isDown = true;
			else if (object.currentY > targetY)
				isUp = true;

			while (!object.deleted) {

				repaint();
				if (isRight) {
					object.currentX += (double) px_per_second / 100;
					if (object.currentX > targetX) {
						isRight = false;
						isLeft = true;
						// swap target and origin
						int temp = object.originalX;
						object.originalX = targetX;
						targetX = temp;
						if (!facingRight)
							object.setImage(flipHorizontal(object.getImage()));
					}
				} else if (isLeft) {
					object.currentX -= (double) px_per_second / 100;
					if (object.currentX < targetX) {
						isLeft = false;
						isRight = true;
						// swap target and origin
						int temp = object.originalX;
						object.originalX = targetX;
						targetX = temp;
						if (facingRight)
							object.setImage(flipHorizontal(object.getImage()));
					}
				}

				if (isDown) {
					object.currentY += (double) px_per_second / 100;
					if (object.currentY > targetY) {
						isDown = false;
						isUp = true;
						// swap target and origin
						int temp = object.originalY;
						object.originalY = targetY;
						targetY = temp;
					}
				} else if (isUp) {
					object.currentY -= (double) px_per_second / 100;
					if (object.currentY < targetY) {
						isUp = false;
						isDown = true;
						// swap target and origin
						int temp = object.originalY;
						object.originalY = targetY;
						targetY = temp;
					}
				}

				try {

					Thread.sleep(10);
				} catch (InterruptedException ex) {

					Logger.getLogger(ImageSurface.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}
		}
	}

	public void circularAnimation(MovingImage fish5, int ceta,
			int px_per_second, int cycleRadius) {
		fish5.circularMotion = true;
		new CircularAnimationRunnable(fish5, ceta, px_per_second, cycleRadius);
	}

	/**
	 * Moves the MovingImage in a circular trajectory
	 * 
	 * @author mkhamis
	 * 
	 */
	class CircularAnimationRunnable implements Runnable {
		private Thread runner;
		private MovingImage object;
		private double ceta;
		private double px_per_second;
		private double cycleRadius;

		/**
		 * 
		 * @param movingObject
		 *            The image to be moved
		 * @param ceta
		 *            the angle at which it starts moving
		 * @param px_per_second
		 *            the object's speed
		 * @param cycleRadius
		 *            how far away is the image from the center of motion
		 */
		public CircularAnimationRunnable(MovingImage movingObject, int ceta,
				int px_per_second, int cycleRadius) {
			this.object = movingObject;
			this.cycleRadius = (double) cycleRadius;
			this.px_per_second = (double) px_per_second;
			this.ceta = (double) ceta;
			this.ceta = (double) ceta;
			initThread();
		}

		private void initThread() {

			runner = new Thread(this);
			runner.start();
		}

		public void run() {

			while (!object.deleted) {
				repaint();
				float lastX = (float) object.currentX;
				float lastY = (float) object.currentY;
				float x = (float) (object.originalX + Math.cos(Math
						.toRadians(ceta)) * cycleRadius);
				float y = (float) (object.originalY + Math.sin(Math
						.toRadians(ceta)) * cycleRadius);

				object.currentX = (int) x;
				object.currentY = (int) y;

				ceta += (px_per_second / 100)
						* (360 / (2 * Math.PI * cycleRadius));

				// System.out.println("last x:" + lastX);
				// System.out.println("last y:" + lastY);
				// System.out.println("current x:" + object.currentX);
				// System.out.println("current y:" + object.currentY);
				//
				// System.out.println("x diff: " + (lastX - object.currentX));
				// System.out.println("y diff: " + (lastY - object.currentY));

				// System.out.println(object.x + ":" + object.y);

				try {

					Thread.sleep(10);
				} catch (InterruptedException ex) {

					Logger.getLogger(ImageSurface.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}

		}
	}

	/**
	 * This runnable runs the thread responsible for the fading away of a
	 * MovingImage.
	 * 
	 * @author mkhamis
	 * 
	 */
	class FadeOutRunnable implements Runnable {
		private Thread runner;
		private MovingImage object;
		private SmoothPursuits mainFrame;

		// The fadeout time in ms
		private static final int FADEOUT_TIME = 300; // in ms

		/**
		 * @param object
		 *            The MovingImage to be faded out
		 * @param mainFrame
		 *            The parent SmoothPursuits object
		 */
		public FadeOutRunnable(MovingImage object, SmoothPursuits mainFrame) {
			this.object = object;
			this.mainFrame = mainFrame;
			initThread();
		}

		private void initThread() {

			runner = new Thread(this);
			runner.start();
		}

		public void run() {
			// stop eye tracking till this process is finished
			mainFrame.pauseEyeTracking();
			while (object.alpha > 0) {

				repaint();
				object.alpha += -0.1f;

				if (object.alpha < 0) {

					object.alpha = 0;
				}

				try {

					Thread.sleep(FADEOUT_TIME / 10);
				} catch (InterruptedException ex) {

					Logger.getLogger(ImageSurface.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}
			// remove image from the smooth pursuits object, so that gaze data
			// is no longer correlated to it
			mainFrame.removeImage(object);
			// resume collecting gaze data
			mainFrame.unpauseEyeTracking();
		}
	}

	/**
	 * This runnable is used to run a thread responsible for blinking the error
	 * message that appears in case no eyes are detected
	 * 
	 * @author mkhamis
	 * 
	 */
	class BlinkingRunnable implements Runnable {
		private Thread runner;

		private JLabel l1;
		private JLabel l2;

		boolean running = true;

		int waitTime = 500;

		synchronized void pause() {
			l1.setVisible(false);
			l2.setVisible(false);
		}

		synchronized void resume() {
			runner.notify();
		}

		public BlinkingRunnable(JLabel l1, JLabel l2) {
			this.l1 = l1;
			this.l2 = l2;
			initThread();
		}

		public boolean isAlive() {
			return runner.isAlive();
		}

		private void initThread() {

			runner = new Thread(this);
			runner.start();
		}

		public void run() {
			while (running) {
				try {
					l1.setVisible(false);
					l2.setVisible(false);
					Thread.sleep(waitTime);
					l1.setVisible(true);
					l2.setVisible(true);
					Thread.sleep(waitTime + 200);
				} catch (InterruptedException ex) {
					Logger.getLogger(ImageSurface.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}

		}
	}

	public int getConditionIndex() {
		return conditionIndex;
	}

	public void setConditionIndex(int conditionIndex) {
		this.conditionIndex = conditionIndex;
	}

}