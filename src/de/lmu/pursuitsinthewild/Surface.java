package de.lmu.pursuitsinthewild;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.apache.commons.math3.analysis.function.Cos;

class Surface extends JPanel {

	// private ZRectangle zrect;
	private ArrayList<MovingObject> movingObjects;

	private SmoothPursuits sp;

	BufferedImage image;

	public Surface() {

		initUI();
	}

	public Surface(SmoothPursuits sp, ArrayList<MovingObject> movingObjects) {

		this.movingObjects = movingObjects;
		this.sp = sp;
		initUI();

		try {
			image = ImageIO.read(new File("./fish_images/colorful.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Animates the given object
	 * 
	 * @param movingObject
	 *            the object to be animated
	 * @param targetX
	 *            the point on the X-axis (in pixels) to which the object should
	 *            move to
	 * @param targetY
	 *            the point on the Y-axis (in pixels) to which the object should
	 *            move to
	 * @param px_per_second
	 *            the numbers of pixels covered per second
	 */
	public void animate(MovingObject movingObject, int targetX, int targetY,
			int px_per_second) {
		new AnimateRunnable(movingObject, targetX, targetY, px_per_second);
	}

	public void animate(MovingObject movingObject, double targetX,
			double targetY, int px_per_second) {
		animate(movingObject, (int) targetX, (int) targetY, px_per_second);
	}

	public void fadeOut(MovingObject movingObject, SmoothPursuits mainFrame) {
		new FadeOutRunnable(movingObject, mainFrame);
	}

	private void initUI() {

		// zrect = new ZRectangle(50, 50, 50, 50);
		// zell = new ZEllipse(150, 70, 50, 50);
		// zell2 = new ZEllipse(600, 800, 50, 50);

	}

	private void doDrawing(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		// Workaround for antialiasing
		Font font = new Font("Serif", Font.BOLD, 40);
		g2d.setFont(font);

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		// g2d.setColor(new Color(0, 0, 200));
		// g2d.fill(zrect);

		for (MovingObject movingObject : movingObjects) {
			try {
				g2d.setColor(movingObject.color);
				g2d.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, movingObject.alpha));
				g2d.fill(movingObject);
			} catch (ConcurrentModificationException e) {
				e.printStackTrace();
			}
		}

		g.drawImage(image.getScaledInstance(200, image.getHeight() * 200
				/ image.getWidth(), Image.SCALE_DEFAULT), 200, 300, null);

	}

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		doDrawing(g);
	}

	class AnimateRunnable implements Runnable {

		private Thread runner;
		private MovingObject object;
		private int targetX;
		private int targetY;
		private int px_per_second;

		public AnimateRunnable(MovingObject object, int targetX, int targetY) {
			this.object = object;
			this.targetX = targetX;
			this.targetY = targetY;
			initThread();
		}

		public AnimateRunnable(MovingObject object, int targetX, int targetY,
				int px_per_second) {
			this.object = object;
			this.targetX = targetX;
			this.targetY = targetY;
			this.px_per_second = px_per_second;
			initThread();
		}

		private void initThread() {

			runner = new Thread(this);
			runner.start();
		}

		@Override
		public void run() {
			boolean isRight = false;
			boolean isLeft = false;
			boolean isDown = false;
			boolean isUp = false;
			if (object.x < targetX)
				isRight = true;
			else if (object.x > targetX)
				isLeft = true;

			if (object.y < targetY)
				isDown = true;
			else if (object.y > targetY)
				isUp = true;

			while (!object.deleted) {

				repaint();
				if (isRight) {
					object.x += px_per_second / 100;
					if (object.x > targetX) {
						isRight = false;
						isLeft = true;
						// swap target and origin
						int temp = object.originalX;
						object.originalX = targetX;
						targetX = temp;
					}
				} else if (isLeft) {
					object.x -= px_per_second / 100;
					if (object.x < targetX) {
						isLeft = false;
						isRight = true;
						// swap target and origin
						int temp = object.originalX;
						object.originalX = targetX;
						targetX = temp;
					}
				}

				if (isDown) {
					object.y += px_per_second / 100;
					if (object.y > targetY) {
						isDown = false;
						isUp = true;
						// swap target and origin
						int temp = object.originalY;
						object.originalY = targetY;
						targetY = temp;
					}
				} else if (isUp) {
					object.y -= px_per_second / 100;
					if (object.y < targetY) {
						isUp = false;
						isDown = true;
						// swap target and origin
						int temp = object.originalY;
						object.originalY = targetY;
						targetY = temp;
					}
				}

				try {

					Thread.sleep(10);
				} catch (InterruptedException ex) {

					Logger.getLogger(Surface.class.getName()).log(Level.SEVERE,
							null, ex);
				}
			}
		}
	}

	public void circularAnimation(MovingObject movingObject, int ceta,
			int px_per_second, int cycleRadius) {
		movingObject.circularMotion = true;
		new CircularAnimationRunnable(movingObject, ceta, px_per_second,
				cycleRadius);
	}

	class CircularAnimationRunnable implements Runnable {
		private Thread runner;
		private MovingObject object;
		private int ceta;
		private int px_per_second;
		private int cycleRadius;

		public CircularAnimationRunnable(MovingObject movingObject, int ceta,
				int px_per_second, int cycleRadius) {
			this.object = movingObject;
			this.cycleRadius = cycleRadius;
			this.px_per_second = px_per_second;
			initThread();
		}

		private void initThread() {

			runner = new Thread(this);
			runner.start();
		}

		@Override
		public void run() {

			while (!object.deleted) {
				repaint();
				float x = (float) (object.originalX + Math.cos(Math
						.toRadians(ceta)) * cycleRadius);
				float y = (float) (object.originalY + Math.sin(Math
						.toRadians(ceta)) * cycleRadius);

				object.x = x;
				object.y = y;

				ceta += px_per_second / 100;

				System.out.println(object.x + ":" + object.y);

				try {

					Thread.sleep(10);
				} catch (InterruptedException ex) {

					Logger.getLogger(Surface.class.getName()).log(Level.SEVERE,
							null, ex);
				}
			}

		}
	}

	class FadeOutRunnable implements Runnable {
		private Thread runner;
		private MovingObject object;
		private SmoothPursuits mainFrame;

		public FadeOutRunnable(MovingObject object, SmoothPursuits mainFrame) {
			this.object = object;
			this.mainFrame = mainFrame;
			initThread();
		}

		private void initThread() {

			runner = new Thread(this);
			runner.start();
		}

		@Override
		public void run() {
			mainFrame.pauseEyeTracking();
			while (object.alpha > 0) {

				repaint();
				object.alpha += -0.1f;

				if (object.alpha < 0) {

					object.alpha = 0;
				}

				try {

					Thread.sleep(50);
				} catch (InterruptedException ex) {

					Logger.getLogger(Surface.class.getName()).log(Level.SEVERE,
							null, ex);
				}
			}
			mainFrame.unpauseEyeTracking();
//			mainFrame.removeObject(object);
		}
	}

}