package de.lmu.pursuitsinthewild;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

class StartSurface extends JPanel {

	static String NONE_EYES = "No Eyes Detected";
	static String LEFT_EYE = "Only Left Eye Detected";
	static String RIGHT_EYE = "Only Right Eye Detected";
	static String BOTH_EYES = "Both Eyes Detected!";
	static String GREEN_MARKER = "Please step into the green marker on the floor";
	static String LOADING = "Loading...";

	private SmoothPursuits sp;
	JLabel title = new JLabel("The Eye Fishing Game");
	JLabel subtitle = new JLabel("fish with your eyes!");

	JLabel eyesDetectedLabel = new JLabel(NONE_EYES); // Eyes Detected!
	JLabel eyesDetectedHint = new JLabel(
			"Please step into the green marker on the floor"); // Loading...

	boolean leftDetected = false;
	boolean rightDetected = false;

	public StartSurface() {

		initUI();
	}

	public StartSurface(SmoothPursuits sp) {

		this.sp = sp;
		initUI();

	}

	/**
	 * Initialize the UI
	 */
	private void initUI() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		int border = (int) (sp.getHeight() * .04);
		Color darkYellow = new Color(179, 153, 15);
		Color maroon = new Color(128, 0, 0);
		Color reddish = new Color(242, 117, 99);
		Color orangish = new Color(255, 179, 57);

		this.setBorder(BorderFactory.createEmptyBorder(border, border,
				border, border));

		System.out.println(sp.getWidth());
		System.out.println(sp.getHeight());

		title.setFont(new Font("Goudy Stout", Font.BOLD,
				(int) (sp.getWidth() * 0.04)));
		title.setForeground(orangish);
		this.add(title);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.04)));

		subtitle.setFont(new Font("Sans Serif", Font.BOLD,
				(int) (sp.getWidth() * 0.025)));
		subtitle.setForeground(orangish);
		this.add(subtitle);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.3)));

		eyesDetectedLabel.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		eyesDetectedLabel.setForeground(reddish);
		this.add(eyesDetectedLabel);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.04)));

		eyesDetectedHint.setFont(new Font("Sans Serif", Font.BOLD, (int) (sp
				.getWidth() * 0.025)));
		eyesDetectedHint.setForeground(reddish);
		this.add(eyesDetectedHint);
		this.add(Box.createVerticalStrut((int) (sp.getHeight() * 0.04)));

		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		subtitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		eyesDetectedLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		eyesDetectedHint.setAlignmentX(Component.CENTER_ALIGNMENT);
	}

	public void showEyes(GazePoint gazePoint) {
		if (gazePoint.getX() > -1) {
			// TODO: show the user's eyes
		}
	}

	/**
	 * Draw the background image
	 * 
	 * @param g
	 */
	private void doDrawing(Graphics g) {
		g.drawImage(new ImageIcon("./images/underwater.jpg").getImage(), 0, 0,
				this.getWidth(), this.getHeight(), null);
	}

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		doDrawing(g);
	}

	/**
	 * Finds which eyes are detected and sets error message accordingly
	 * 
	 * @param isLeftDetected
	 * @param isRightDetected
	 * @return true only when both eyes are detected
	 */
	public boolean setEyesDetected(boolean isLeftDetected,
			boolean isRightDetected) {

		if (isLeftDetected && !leftDetected) {
			leftDetected = true;
			eyesDetectedLabel.setText(LEFT_EYE);
		} else if (!isLeftDetected && leftDetected) {
			leftDetected = false;
		}

		if (isRightDetected && !rightDetected) {
			rightDetected = true;
			eyesDetectedLabel.setText(RIGHT_EYE);
		} else if (!isRightDetected && rightDetected) {
			rightDetected = false;
		}

		if (leftDetected && rightDetected) {
			eyesDetectedLabel.setText(BOTH_EYES);
			eyesDetectedHint.setText(LOADING);
			return true;
			
		} else if (!leftDetected && !rightDetected) {
			eyesDetectedLabel.setText(NONE_EYES);
			eyesDetectedHint.setText(GREEN_MARKER);
		}
		return false;
	}

	public void setLeftDetected(boolean isDetected) {
		if (isDetected && !leftDetected) {
			leftDetected = true;
			eyesDetectedLabel.setText(LEFT_EYE);
			setBothDetected();
		} else if (!isDetected && leftDetected) {
			leftDetected = false;
			if (rightDetected) {
				eyesDetectedLabel.setText(RIGHT_EYE);
			} else {
				eyesDetectedLabel.setText(NONE_EYES);
			}
		}

	}

	public void setRightDetected(boolean isDetected) {
		if (isDetected && !rightDetected) {
			rightDetected = true;
			eyesDetectedLabel.setText(RIGHT_EYE);
			setBothDetected();
		} else if (!isDetected && rightDetected) {
			rightDetected = false;
			if (leftDetected) {
				eyesDetectedLabel.setText(LEFT_EYE);
			} else {
				eyesDetectedLabel.setText(NONE_EYES);
			}
		}
		sp.repaint();
	}

	public void setBothDetected() {
		if (rightDetected && leftDetected) {
			eyesDetectedLabel.setText(BOTH_EYES);
			eyesDetectedHint.setText(LOADING);
			// TODO: launch level one
		}
	}

}