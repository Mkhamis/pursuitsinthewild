package de.lmu.pursuitsinthewild;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

class MovingImage {

	// The original X and Y coordinates in which the image appeared the first
	// time
	int originalX;
	int originalY;

	// The current X and Y coordinates of the image
	double currentX;
	double currentY;

	// The width and height of the image
	int width;
	int height;

	// true if this image is already removed from the UI
	boolean deleted = false;

	// true if the image is to move in a circular trajectory
	boolean circularMotion = false;

	// a variable used to manipulate the transparency of the image
	float alpha;

	// a name to help debugging
	String name = "";
	// the path of the image
	String path;

	// a static counter of MovingImages created so far
	static int counter = 1;

	// an index of the MovingImage, used for debugging
	int index;

	// The most recent calculated correlation between this moving image and tha
	// gaze data
	java.lang.Double correlation;

	// an array of horizontal gaze positions collected so far in this Window
	private ArrayList<java.lang.Double> objectXCoordinates;
	// an array of vertical gaze positions collected so far in this Window
	private ArrayList<java.lang.Double> objectYCoordinates;

	// The image
	private BufferedImage image;

	/**
	 * Creates a new MovingImage
	 * 
	 * @param x
	 *            the orignal positiono on x-axis
	 * @param y
	 *            the orignal positiono on y-axis
	 * @param width
	 *            width of the image
	 * @param height
	 *            height of the image
	 * @param path
	 *            the image's path
	 * @param name
	 *            the name used for debugging
	 */
	public MovingImage(int x, int y, int width, int height, String path,
			String name) {

		this.name = name;
		this.originalX = (int) x;
		this.originalY = (int) y;
		this.currentX = (int) x;
		this.currentY = (int) y;
		this.width = width;
		this.height = height;
		this.alpha = 1f;
		this.objectXCoordinates = new ArrayList<java.lang.Double>();
		this.objectYCoordinates = new ArrayList<java.lang.Double>();
		this.index = MovingImage.counter;
		MovingImage.counter++;

		this.image = null;

		// Read the image and load it into the panel
		try {
			BufferedImage originalImage = ImageIO.read(new File(path));
			this.height = this.width * originalImage.getHeight()
					/ originalImage.getWidth();
			this.image = new BufferedImage(this.width, this.height,
					BufferedImage.TYPE_INT_ARGB);
			// The following lines are to convert the Image to a BufferedImage..
			// No.. casting does not work!
			Graphics g = this.image.getGraphics();
			g.drawImage(originalImage.getScaledInstance(this.width,
					this.height, BufferedImage.SCALE_DEFAULT), 0, 0, null);
			g.dispose();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public ArrayList<java.lang.Double> getObjectXCoordinates() {
		return objectXCoordinates;
	}

	public ArrayList<java.lang.Double> getObjectYCoordinates() {
		return objectYCoordinates;
	}

	public void logCoordinates() {
		this.objectXCoordinates.add(new java.lang.Double(this.currentX));
		this.objectYCoordinates.add(new java.lang.Double(this.currentY));
	}

	public void clearCoordinates() {
		this.objectXCoordinates = new ArrayList<java.lang.Double>();
		this.objectYCoordinates = new ArrayList<java.lang.Double>();
	}

	/**
	 * Goes through all MovingImages and returns the oen with the highest
	 * correlation with the gaze data
	 * 
	 * @param movingImages list of MovingImages
	 * @return A MovingImage with highest correlation with gaze data
	 */
	public static MovingImage getMaxCorrelation(
			ArrayList<MovingImage> movingImages) {
		if (movingImages.size() == 0)
			return null;
		MovingImage maxSoFar = movingImages.get(0);
		for (MovingImage movingObject : movingImages) {
			if (movingObject.correlation > maxSoFar.correlation)
				maxSoFar = movingObject;
		}
		return maxSoFar;

	}

	public BufferedImage getImage() {
		return this.image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	/**
	 * Return image to its initial state in terms of transparency and deletion.
	 */
	public void resetImage() {
		this.alpha = 1.0f;
		this.deleted = false;
	}

}
