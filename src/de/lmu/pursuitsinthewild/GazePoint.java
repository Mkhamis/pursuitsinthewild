package de.lmu.pursuitsinthewild;

/**
 * This class represents a gaze point
 * @author mkhamis
 *
 */
public class GazePoint {
	
	// the gaze position on the x-axis
	private double x;
	// the gaze position on the y-axis
	private double y;

	public GazePoint() {
		this.x = 0.0;
		this.y = 0.0;
	}

	/**
	 * Initialize a new GazePoint
	 * @param x position on x-axis
	 * @param y position on y-axis
	 */
	public GazePoint(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Initialize a new GazePoint
	 * @param x position on x-axis
	 * @param y position on y-axis
	 */
	public GazePoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public String toString(){
		return this.x+", "+this.y;
	}

}
