Here are the steps to run this project

* Import the project to eclipse's workspace
* You need to import this as a project in your Eclipse workspace, and refer to it in the java build path of the pursuits project: https://github.com/ralfbiedert/tobiisdk4j/
* You'll also need to import these two jar files into your java build path:

* * http://commons.apache.org/proper/commons-math/download_math.cgi
* * https://commons.apache.org/proper/commons-csv/download_csv.cgi